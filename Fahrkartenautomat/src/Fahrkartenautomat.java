﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       
       //save the types and prices here for easy change in case of such need
       final int amount = 10;
       final double[] price = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
       final String[] type = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
    		   				  "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
    		   				  "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
    		   				  "Kleingruppen-Tageskarte Berlin ABC"};
       
       do {
    	   //to sign that the vending machine is ready to sell the tickets
    	   String eingabetaste;
    	   do {
    		   System.out.println("Drücken Sie die Eingabetaste, um ein Ticket zu kaufen.");
    		   eingabetaste = tastatur.nextLine();
    		   } while (!eingabetaste.equals(""));
    	   
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen(amount, price, type);
    	   rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);
    	   fahrkartenAusgeben();
    	   rueckgeldAusgeben(rückgabebetrag);
       
    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          	  "vor Fahrtantritt entwerten zu lassen!\n"+
                          	  "Wir wünschen Ihnen eine gute Fahrt.\n\n\n");
       }
       while(true);
    }
    
    
   //to order the required ticket
    public static double fahrkartenbestellungErfassen(int amount, double[] price, String[] type) {
    	Scanner tastatur = new Scanner(System.in);
        double zuZahlenderBetrag = 0;
    	int wahl = 0;
    	
    	System.out.println("Fahrkartenbestellvorgang:\n"
           		+ "=========================\n"
           		+ "\n"
           		+ "Wählen Sie:\n"
           		+ "  Bezahlen (0)");
        for (int i = 0; i < amount; i++) {
        	System.out.print("  " + type[i] + " - " + price[i] + " € (" + (i+1) + ")\n");
        	}
        
    	do {
        
    		//to get a type of a tickets
    		System.out.print("\nIhre Wahl: ");
    		wahl = tastatur.nextInt();
    		while (wahl < 0 || wahl > 10)
    		{
    			System.out.println(">>falsche Eingabe<<");
    			System.out.print("Ihre Wahl: ");
    			wahl = tastatur.nextInt();
    		}
        
    		if (wahl == 0) {
    			zuZahlenderBetrag = zuZahlenderBetrag;
    		} else {
    		//Anzahl der Fahrkarten geben
            //Fahrkartenautomat-05++
             int tickets = 0;
             do {
          	   System.out.print("Anzahl der Tickets (1-10): ");
          	   tickets = tastatur.nextInt();
          	   
             }
              while(tickets <= 0 || tickets > 10);
             
              zuZahlenderBetrag = zuZahlenderBetrag + price[wahl - 1]*tickets;
              
              System.out.print("Fortsetzen zu kaufen.");
    		}  
        
    	} while (wahl != 0);
    	
    	return zuZahlenderBetrag;
    }
    
    //receive a paid sum and return a return amount
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.print("Noch zu zahlen: ");
     	   //Aufgabe 7
     	   System.out.printf("%.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void warte(int millisekunde) {
    	try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	//to avoid the inaccuracy because of double-type
        rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
        
        if(rückgabebetrag >  0.0)
        {
     	   System.out.print("Der Rückgabebetrag in Höhe von ");
     	   
     	   
     	   System.out.printf("%.2f", rückgabebetrag);
     	   
     	   System.out.println(" EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
  	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT ");
  	          rückgabebetrag -= 0.05;
  	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
        }
    }
}