import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x;
      double y;
      double m = 0;
      
      Scanner scan = new Scanner(System.in);
      
      int anzahl = 0;
      System.out.print("Bitte geben Sie die Anzahl ein: ");
      anzahl = scan.nextInt();
  
      //System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
      //x = scan.nextDouble();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      
      for (int i = anzahl; i > 0; i--) {
    	  System.out.print("Bitte geben Sie eine Zahl ein: ");
          x = scan.nextDouble();
    	  m = m + x;
      }
      m = m/anzahl;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      //System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      System.out.printf("Der Mittelwert ist %.2f\n", m);
   }
}