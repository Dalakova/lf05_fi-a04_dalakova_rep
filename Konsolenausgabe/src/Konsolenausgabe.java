//Aufgabe 1
public class Konsolenausgabe {
	
	public static void main(String[] args) {
		
		System.out.println("Aufgabe 1\n");

		System.out.println("   **\n" +
						   "*      *\n" + 
						   "*      *\n" + 
						   "   **");
		
		//Aufgabe 2
		System.out.println("\n\nAufgabe 2\n");
		Fakultaet.print(5);
		
		//Aufgabe 3
		System.out.println("\n\nAufgabe 3");
		TemperatureTabelle.tabelle();
		System.out.println("\n");
	}

}
