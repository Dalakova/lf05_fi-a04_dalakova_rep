//Aufgabe 3
public class TemperatureTabelle {
	
	//the data saved in variables
	static int[] fahrenheit = {-20, -10, 0, 20, 30};
	static double[] celsius = {-28.8889, -23.3333, -17.7778, -6.6667, -1.1111};
	static String f = "Fahrenheit";
	static String c = "Celsius";
	
	//to print the data
	public static void tabelle( ) {
		
		//to print the table's header
		System.out.printf("\n%-12s|%10s\n", f, c);
		
		//to print the line between header and body
		for (int i = 0; i < 24; i ++ ) {
			System.out.print("-");
		}
		
		//to print the table
		for (int i = 0; i < 5; i++) {
			System.out.printf("\n%+-12d|%10.2f", fahrenheit[i], celsius[i]);
		}
	};

}
