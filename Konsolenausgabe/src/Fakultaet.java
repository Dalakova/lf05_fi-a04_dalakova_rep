//Aufgabe 2
public class Fakultaet {
	//to calculate a factorial
	public static int calc (int x) {
		int f = 1;
		for (int i = x; i>0; i--) {
			f = f*i;
		}
		return f;
	}
	//to print results
	public static void print(int n) {
		int x = n;
		for (int j = n; j>=0; j--) {
			
			//to print "x! ="
			System.out.print((n - j) + "! =");
			
			//to make a string " 1 * 2 *" etc
			String s = "";
			
			for (int i = 1; i < n-j; i++) {
				s = s + " " + i + " *";
			}
			if ((n - j) > 1 ) {
				s = s + " " + (n-j);
			}
			//to print this string
			System.out.print(s);
			
			//to print appropriate amount of " " and " = "
			for (int i = 0; i < (4*n - 2) - s.length(); i++) {
				System.out.print(" ");
			}
			System.out.print(" = ");
			
			//to find the length of the results and print appropriate amount of " " after "="
			String result = String.valueOf(calc(x-j));
			String maxResult = String.valueOf(calc(n));
			for (int i = 0; i < (maxResult.length() - result.length()); i ++) {
				System.out.print(" ");
			}
			
			//to print result
			System.out.println(result);
		}
	}
}
